package com.aardwark.kw.domain_events.item_type;

public enum EventType {
    CREATED,
    MODIFIED,
    STATUS_CHANGE
}
