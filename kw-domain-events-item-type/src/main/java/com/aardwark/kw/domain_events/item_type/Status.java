package com.aardwark.kw.domain_events.item_type;

public enum Status {
    NEW,
    ACTIVE,
    DEACTIVATED,
    DELETED
}
