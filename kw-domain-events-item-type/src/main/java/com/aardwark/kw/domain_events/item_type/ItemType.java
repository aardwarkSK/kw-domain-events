package com.aardwark.kw.domain_events.item_type;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class ItemType {

    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    private UUID id;

    @JsonProperty("status")
    private Status status;

    @JsonProperty("creation_date")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime creationDate;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ItemType itemType = (ItemType) o;
        return Objects.equals(name, itemType.name) && Objects.equals(id,
                                                                     itemType.id)
               && status == itemType.status && Objects.equals(creationDate,
                                                              itemType.creationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, status, creationDate);
    }

    @Override
    public String toString() {
        return "ItemType{" +
               "name='" + name + '\'' +
               ", id=" + id +
               ", status=" + status +
               ", creationDate=" + creationDate +
               '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }
}
