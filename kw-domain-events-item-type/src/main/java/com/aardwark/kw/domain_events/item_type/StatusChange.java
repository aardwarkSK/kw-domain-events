package com.aardwark.kw.domain_events.item_type;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

public class StatusChange {

    @JsonProperty("item_type_id")
    private UUID itemTypeId;

    @JsonProperty("old_status")
    private Status oldStatus;

    @JsonProperty("new_status")
    private Status newStatus;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        StatusChange that = (StatusChange) o;
        return Objects.equals(itemTypeId, that.itemTypeId) && oldStatus == that.oldStatus
               && newStatus == that.newStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemTypeId, oldStatus, newStatus);
    }

    @Override
    public String toString() {
        return "StatusChange{" +
               "itemTypeId=" + itemTypeId +
               ", oldStatus=" + oldStatus +
               ", newStatus=" + newStatus +
               '}';
    }

    public UUID getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(UUID itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public Status getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(Status oldStatus) {
        this.oldStatus = oldStatus;
    }

    public Status getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(Status newStatus) {
        this.newStatus = newStatus;
    }
}
