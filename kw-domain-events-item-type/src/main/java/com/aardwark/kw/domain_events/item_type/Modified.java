package com.aardwark.kw.domain_events.item_type;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

public class Modified {

    @JsonProperty("item_type_id")
    private UUID itemTypeId;

    @JsonProperty("original_name")
    private String originalName;

    @JsonProperty("new_name")
    private String newName;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Modified modified = (Modified) o;
        return Objects.equals(itemTypeId, modified.itemTypeId) && Objects.equals(
            originalName, modified.originalName) && Objects.equals(newName, modified.newName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemTypeId, originalName, newName);
    }

    @Override
    public String toString() {
        return "Modified{" +
               "itemTypeId=" + itemTypeId +
               ", originalName='" + originalName + '\'' +
               ", newName='" + newName + '\'' +
               '}';
    }

    public UUID getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(UUID itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }
}
