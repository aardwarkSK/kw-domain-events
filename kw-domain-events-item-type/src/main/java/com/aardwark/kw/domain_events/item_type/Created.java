package com.aardwark.kw.domain_events.item_type;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Created {

    @JsonProperty("item_type")
    private ItemType itemType;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Created created = (Created) o;
        return Objects.equals(itemType, created.itemType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemType);
    }

    @Override
    public String toString() {
        return "Created{" +
               "itemType=" + itemType +
               '}';
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }
}
