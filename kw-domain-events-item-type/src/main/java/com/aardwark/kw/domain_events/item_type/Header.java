package com.aardwark.kw.domain_events.item_type;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

public class Header {

    @JsonProperty("event_type")
    private EventType eventType;

    @JsonProperty("item_type_id")
    private UUID itemTypeId;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Header header = (Header) o;
        return eventType == header.eventType && Objects.equals(itemTypeId, header.itemTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventType, itemTypeId);
    }

    @Override
    public String toString() {
        return "Header{" +
               "eventType=" + eventType +
               ", itemTypeId=" + itemTypeId +
               '}';
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public UUID getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(UUID itemTypeId) {
        this.itemTypeId = itemTypeId;
    }
}
