package com.aardwark.kw.domain_events.item_type;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.UUID;

public class ItemTypeDeleted {

    @JsonProperty("item_type_id")
    private UUID itemTypeId;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        ItemTypeDeleted that = (ItemTypeDeleted) o;
        return Objects.equals(itemTypeId, that.itemTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemTypeId);
    }

    @Override
    public String toString() {
        return "ItemTypeDeleted{" +
               "itemTypeId=" + itemTypeId +
               '}';
    }

    public UUID getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(UUID itemTypeId) {
        this.itemTypeId = itemTypeId;
    }
}
