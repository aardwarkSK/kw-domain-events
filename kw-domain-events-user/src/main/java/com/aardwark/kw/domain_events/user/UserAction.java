package com.aardwark.kw.domain_events.user;

public enum UserAction {
    CREATED,
    UPDATED,
    STATUS_CHANGED
}
