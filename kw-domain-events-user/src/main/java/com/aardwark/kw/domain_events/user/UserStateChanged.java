package com.aardwark.kw.domain_events.user;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Data
@NoArgsConstructor
@SuperBuilder
public class UserStateChanged implements UserEvent {
    private UUID id;
    private String oldState;
    private String newState;
}
