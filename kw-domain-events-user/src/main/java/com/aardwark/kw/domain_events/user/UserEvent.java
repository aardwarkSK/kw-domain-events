package com.aardwark.kw.domain_events.user;

import java.util.UUID;

public interface UserEvent {

    UUID getId();
}
