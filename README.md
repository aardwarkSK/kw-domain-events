# Kafka Workshop Domain Events

## Maven Settings.xml

**Note:** nexus.infra.aardwark.com is accessible from Aardwark VPN

[Maven Settings.xml](https://maven.apache.org/settings.html)

```xml
<settings>
    <profiles>
        <profile>
            <id>aardwark</id>
            <repositories>
                <repository>
                    <id>central</id>
                    <url>http://nexus.infra.aardwark.com:8081/repository/maven-public/</url>
                    <layout>default</layout>
                </repository>
            </repositories>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>aardwark</activeProfile>
    </activeProfiles>
</settings>
```