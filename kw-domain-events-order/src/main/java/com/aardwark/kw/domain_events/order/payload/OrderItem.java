package com.aardwark.kw.domain_events.order.payload;

import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Builder
@Value
public class OrderItem {
    UUID itemTypeId;
    Integer quantity;
}
