package com.aardwark.kw.domain_events.order;

import com.aardwark.kw.domain_events.order.payload.Order;
import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Builder
@Value
public class OrderDelivered {
    UUID orderId;
    Order payload;
}
