package com.aardwark.kw.domain_events.store_item;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Data
@SuperBuilder
@RequiredArgsConstructor
public abstract class ItemQuantityChanged {

    private UUID itemTypeId;

    private Long quantity;

    private Long totalQuantity;

}
